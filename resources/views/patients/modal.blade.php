<div class="modal fade" id="modal_delete{{$patient->id}}">
  <div class="modal-dialog">
      <div class="modal-content">
        <form id="deleteuser" style="display:inline" method="POST" action="{{route('paciente.destroy',$patient->id)}}">
        @method('DELETE')
        @csrf

          <div class="modal-header">
              <h4 class="modal-title">Eliminar usuario {{$patient->name}}</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
          </div>
          <div class="modal-body">
              <p>Confirme si desea eliminar el usuario {{$patient->id}}</p>
          </div>
          <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
              <button class="btn btn-danger" type="submit" class="btn btn-primary" >Eliminar</button>
          </div>                     
        </form>
      </div>
      <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->