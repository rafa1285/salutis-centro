@extends('layouts.app')
@section('contenido')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper py-3">
	  <!-- Main content -->
	@if(session()->has('info'))
		<div class="alert alert-success">{{session('info')}}</div>
	@endif
    <section class="content">      	
    	<div class="card">
            <div class="card-header">
            	<h3 class="card-title">Editar pacient: {{$patient -> name.' ' .$patient -> surname.' ' .$patient -> second_surname}}</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">	
            	<form  method="POST" action="{{ route('paciente.update', $patient-> id) }}">
				@method('PATCH')
				
				@include('patients.form', ['btnText' => 'Editar'])
				</form>
			</div>
			<!-- /.card-body -->
		</div>
		<!-- /.card -->
	</section>
</div>
@endsection