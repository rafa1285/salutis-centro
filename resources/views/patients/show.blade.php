@extends('layouts.app')
@section('contenido')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper py-3">
	  <!-- Main content -->
    <section class="content">      	
    	<div class="card">
            <div class="card-header">
            	<h3 class="card-title">{{$patient -> name.' ' .$patient -> surname.' ' .$patient -> second_surname}}</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">			
				<img class="py-2" width="100px" src="../img/fotoCV.jpg">
				<div class="row float-right py-3 px-3">
					<a href="{{route('paciente.edit', $patient->id)}}"><i class="far fa-edit px-4" style="font-size: 25px"></i></a>
					<a href="" data-target="#modal_delete{{$patient->id}}" data-toggle="modal"><i class="far fa-trash-alt px-3" style="font-size: 25px"></i></a>
					@include('patients.modal')
				</div>					
				<div class="row">
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
						<div class="form-group">
							<label for = "nombre"> DNI</label>
							<!-- name hace referencia al parametro llamado nombre que es recibido por categoria form Request -->
							<input type="text" name="dni" value="{{$patient -> dni}}" class="form-control"disabled style= "background-color:#FFFFFF ">
						</div>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
						<div class="form-group">
							<label for = "name"> Nombre</label>
							<!-- name hace referencia al parametro llamado nombre que es recibido por categoria form Request -->
							<input type="text" name="name" value="{{$patient -> name}}" class="form-control"disabled style="background-color:#FFFFFF" >
						</div>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
						<div class="form-group">
							<label for = "surname">Apellido</label>
							<!-- name hace referencia al parametro llamado nombre que es recibido por categoria form Request -->
							<input type="text" name="surname" value="{{$patient -> surname}}" class="form-control"disabled style="background-color:#FFFFFF">
						</div>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
						<div class="form-group">
							<label for = "second_surname">Segundo apellido</label>
							<!-- name hace referencia al parametro llamado nombre que es recibido por categoria form Request -->
							<input type="text" name="second_surname"  value="{{$patient -> second_surname}}" class="form-control" disabled style="background-color:#FFFFFF">
						</div>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
						<div class="form-group">
							<label for = "birthdate">Fecha de nacimiento</label>
							<!-- name hace referencia al parametro llamado nombre que es recibido por categoria form Request -->
							<input type="text" name="birthdate" value="{{$patient -> birthdate}}" class="form-control" disabled style="background-color:#FFFFFF">
						</div>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
						<div class="form-group">
							<label for = "address">Direccion</label>
							<!-- name hace referencia al parametro llamado nombre que es recibido por categoria form Request -->
							<input type="text" name="address" value="{{$patient -> address}}" class="form-control" disabled style="background-color:#FFFFFF">
						</div>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
						<div class="form-group">
							<label for = "phone">Telefono</label>
							<!-- name hace referencia al parametro llamado nombre que es recibido por categoria form Request -->
							<input type="text" name="phone" value="{{$patient -> phone}}" class="form-control" disabled style="background-color:#FFFFFF">
						</div>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
						<div class="form-group">
							<label for = "email">Email</label>
							<!-- name hace referencia al parametro llamado nombre que es recibido por categoria form Request -->
							<input type="text" name="email" required value="{{$patient -> email}}" class="form-control"disabled style="background-color:#FFFFFF">
						</div>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
						<div class="form-group">
							<label for = "reason_visit">Email</label>
							<!-- name hace referencia al parametro llamado nombre que es recibido por categoria form Request -->
							<input type="text" name="reason_visit" required value="{{$patient -> reason_visit}}" class="form-control"disabled style="background-color:#FFFFFF">
						</div>
					</div>												
				</div>
				<!-- /.row -->
			</div>
			<!-- /.card-body -->
		</div>
		<!-- /.card -->
	</section>
</div>
@endsection