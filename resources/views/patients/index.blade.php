@extends('layouts.app')
@section('contenido')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper py-4">
    <!-- Main content -->
    <section class="content">
      	<div class="row">
        	<div class="col-12">
		        <div class="card">
		            <div class="card-header">
		            	<h3 class="card-title py-3">Pacientes</h3>
		              	<div class="row float-right py-3 px-3">
							<a href="{{route('paciente.create')}}"><i class="fas fa-plus" style="font-size: 20px"></i></a>						
						</div>
		            </div>
		            <!-- /.card-header -->
		            <div class="card-body">
		     			<table id="user" class="table table-bordered table-striped">
					        <thead>
				                <tr>
									<th>DNI</th>
									<th>Nombre</th>
									<th>Apellidos</th>
									<th>Telefono</th>
									<th>Email</th>
									<th>Profesional</th>
									<th>Opciones</th>
								</tr>
							</thead>
							<tbody>								
								@foreach ($patients as $patient)
								<tr>
									<td>{{$patient -> dni}}</td>	
									<td>{!!$patient->present()->link()!!}</td>
									<td>{{$patient -> surname.' ' .$patient -> second_surname}}</td>
									<td>{{$patient -> phone}}</td>	
									<td>{{$patient -> email}}</td>
									<td>{{$patient->present()->user()}}</td>										
									<td>										 
										<a href="{{route('paciente.edit', $patient->id)}}"><i class="far fa-edit px-2"></i></a>
										<a href="" data-target="#modal_delete{{$patient->id}}" data-toggle="modal"><i class="far fa-trash-alt px-2"></i></a>
									</td>
								</tr>
								@include('patients.modal')
								@endforeach
							</tbody>					
						</table>
					</div>		
				</div>
			</div>
		</div>
	</section>
@endsection