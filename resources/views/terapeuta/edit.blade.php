<!-- Esta seccion servira para editar una nueva categoria -->
@extends('layouts.app')
@section('contenido')
	<div class="row">
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<h3>Editar Terapeuta: {{ $terapeuta -> nombre. ' ' .$terapeuta -> apellido1}} </h3>
			<!-- Si no tenemos errores no mostramos el div -->
			@if (count($errors)>0)
			<div class="alert alert-danger">
				<ul>
					@foreach($errors -> all() as $error)
					<!-- error lo vamos a recibir de nuestro archivo request -->
					<li>{{$error}}</li>
					@endforeach
				</ul>
			</div>
			@endif
		</div>
	</div>
			<!-- Creamos un formulario para poder editar categorias
			Enviamos los parametros de nuestro formulario. Le enviamos . El metodo al ser PATCh va a llamar a la funcion update, si es patch a update, si es delete a destroy  -->
			
			<form  method="POST" action="{{ route('terapeuta.update', $terapeuta-> id_terapeuta) }}">
			@method('PATCH')
			@csrf 
			<div class="row">
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
					<div class="form-group">
						<label for = "nombre"> DNI</label>
						<!-- name hace referencia al parametro llamado nombre que es recibido por categoria form Request -->
						<input type="text" name="dni" required value="{{$terapeuta -> dni}}" class="form-control" placeholder="DNI...">
					</div>
				</div>
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
					<div class="form-group">
						<label for = "nombre"> Nombre</label>
						<!-- name hace referencia al parametro llamado nombre que es recibido por categoria form Request -->
						<input type="text" name="nombre" required value="{{$terapeuta -> nombre}}" class="form-control" placeholder="Nombre...">
					</div>
				</div>
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
					<div class="form-group">
						<label for = "codigo">Apellido 1</label>
						<!-- name hace referencia al parametro llamado nombre que es recibido por categoria form Request -->
						<input type="text" name="apellido1" required value="{{$terapeuta -> apellido1}}" class="form-control" placeholder="Apellido1...">
					</div>
				</div>
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
					<div class="form-group">
						<label for = "codigo">Apellido2</label>
						<!-- name hace referencia al parametro llamado nombre que es recibido por categoria form Request -->
						<input type="text" name="apellido2"  value="{{$terapeuta -> apellido2}}" class="form-control" placeholder="Apellido2...">
					</div>
				</div>
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
					<div class="form-group">
						<label>Especialidad</label>						
						<select name="especialidad_id" class="form-control">
							@foreach($especialidades as $espe)
							@if($espe->id_especialidad == $terapeuta->especialidad_id)
								<option value="{{$espe->id_especialidad}}"selected>{{$espe->especialidad}}</option>
							@else
								<option value="{{$espe->id_especialidad}}">{{$espe->especialidad}}</option>
							@endif
							@endforeach
						</select>
					</div>
				</div>
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
					<div class="form-group">
						<label for = "codigo">Fecha de nacimiento</label>
						<!-- name hace referencia al parametro llamado nombre que es recibido por categoria form Request -->
						<input type="text" name="fecha_nacimiento" required value="{{$terapeuta -> fecha_nacimiento}}" class="form-control" placeholder="Fecha de nacimiento...">
					</div>
				</div>
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
					<div class="form-group">
						<label for = "nombre">Direccion</label>
						<!-- name hace referencia al parametro llamado nombre que es recibido por categoria form Request -->
						<input type="text" name="direccion"value="{{$terapeuta -> direccion}}" class="form-control" placeholder="Direccion...">
					</div>
				</div>
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
					<div class="form-group">
						<label for = "nombre">Telefono</label>
						<!-- name hace referencia al parametro llamado nombre que es recibido por categoria form Request -->
						<input type="text" name="telefono" required value="{{$terapeuta -> telefono}}" class="form-control" placeholder="Telefono...">
					</div>
				</div>
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
					<div class="form-group">
						<label for = "codigo">Email</label>
						<!-- name hace referencia al parametro llamado nombre que es recibido por categoria form Request -->
						<input type="text" name="email" required value="{{$terapeuta -> email}}" class="form-control" placeholder="Email...">
					</div>
				</div>
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
					<div class="form-group">
						
					</div>

				</div>

				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
					<div class="form-group">
						<button class="btn btn-primary" type="submit">Guardar</button>
						<button class="btn btn-danger" type="reset">Cancelar</button>
					</div>
				</div>

			</div>

			</form>
	
@endsection