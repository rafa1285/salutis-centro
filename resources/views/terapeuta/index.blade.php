<!-- Este index va a mostrar la pantalla de categorias con las categorias existentes -->
@extends('layouts.app')
@section('contenido')
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper py-4">
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Listado de terapeutas</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
     	<table id="terapeuta" class="table table-bordered table-striped">
	        <thead>
                <tr>
					<th>Nombre</th>
					<th>Apellidos</th>
					<th>Fecha de nacimiento</th>
					<th>Direccion</th>
					<th>Telefono</th>
					<th>Email</th>
					<th>Especialidad</th>
					<th>Opciones</th>
				</tr>
			</thead>
			<tbody>
				<!-- $terapeutas hace referencia a la variable que recibimos desde el controlador-->
				@foreach ($terapeutas as $tera)
				<tr>
					<td>{{$tera -> nombre}}</td>
					<td>{{$tera -> apellido1.' ' .$tera -> apellido2}}</td>
					<td>{{$tera -> fecha_nacimiento}}</td>
					<td>{{$tera -> direccion}}</td>
					<td>{{$tera -> telefono}}</td>
					<td>{{$tera -> email}}</td>							
					<td>{{$tera -> especialidad}}</td>
					<td>
						<a href="{{route('terapeuta.edit', $tera->id_terapeuta)}}"><i class="far fa-edit px-2"></i></a>
						<a href="" data-target="#modal_delete{{$tera->id_terapeuta}}" data-toggle="modal"><i class="far fa-trash-alt px-2"></i></a>
					</td>
				</tr>
				@include('terapeuta.modal')
				@endforeach
			</tbody>
			<tfoot>
                <tr>
					<th>Nombre</th>
					<th>Apellidos</th>
					<th>Fecha de nacimiento</th>
					<th>Direccion</th>
					<th>Telefono</th>
					<th>Email</th>
					<th>Especialidad</th>
					<th>Opciones</th>
				</tr>
			</tfoot>
					</table>
	</div>
	
</div>
</div>
</div>
</section>
@endsection