<div class="modal fade" id="modal_delete{{$tera->id_terapeuta}}">
	<div class="modal-dialog">
  		<div class="modal-content">
            <div class="modal-header">
              	<h4 class="modal-title">Eliminar terapeuta {{$tera->nombre}}</h4>
              	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                	<span aria-hidden="true">&times;</span>
              	</button>
            </div>
            <div class="modal-body">
              	<p>Confirme si desea eliminar el terapeuta {{$tera->nombre}}</p>
            </div>
            <div class="modal-footer justify-content-between">
              	<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
              	<button class="btn btn-danger" type="button" class="btn btn-primary" onclick="document.getElementById('terapeuta.destroy').submit()">Eliminar</button>
            </div>
            <form  id="terapeuta.destroy" method="POST" action="{{ route('terapeuta.destroy', $tera-> id_terapeuta) }}">
			@method('DELETE')
			@csrf 
			</form>
      	</div>
      	<!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->