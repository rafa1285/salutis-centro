@csrf
<span class="login100-form-title">Salutis Centro</span>
<span class="login100-form-subtitle">{{$subtitulo}}</span>
@yield('contentForm')
<div class="wrap-input100 validate-input{{ $errors->has('email') ? ' has-error' : '' }}">
    <input class="input100" type="email" name="email" value="{{ old('email') }}" placeholder="Email"> 
        @include('partials.validationErrors', ['errorName' => 'email'])         
        @include('auth.layouts._iconemail')  
</div>
<div class="wrap-input100 validate-input{{ $errors->has('password') ? ' has-error' : '' }}">
    <input class="input100" type="password" name="password" placeholder="Password"> 
        @include('partials.validationErrors', ['errorName' => 'password'])
        @include('auth.layouts._iconpassword') 
</div>
@yield('contentForm2')

<div class="container-login100-form-btn">
    <button type="submit" class="login100-form-btn">{{$btnText}}</button>
</div>
@yield('contentForm3')