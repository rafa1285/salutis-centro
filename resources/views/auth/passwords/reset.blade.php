@extends('layouts.login')
@section('content')
<div class="limiter">
    <div class="container-login100">
        <div class="wrap-login100">
            @include('auth.layouts._logo')
            <form class="login100-form validate-form" method="POST" action="{{ route('password.update') }}">
                @section('contentForm2')
                <div class="wrap-input100 validate-input{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                    <input id="password-confirm" type="password" class="input100" name="password_confirmation" required autocomplete="new-password" placeholder="Repite Password">                    
                    @include('auth.layouts._iconpassword') 
                </div>
                @endsection
                @section('contentForm3')
                 <a class="btn btn-link btn-block" href="{{ route('login') }}">Volver a Login</a>   
                @endsection
                @include('auth.layouts._form', ['btnText' => 'Resetear Contraseña', 'subtitulo' => 'Resetear contraseña'])               
            </form>           
        </div>
    </div>
</div>
@endsection
