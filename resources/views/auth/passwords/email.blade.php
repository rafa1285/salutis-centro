@extends('layouts.login')
@section('content')
<div class="limiter">
    <div class="container-login100">
        <div class="wrap-login100">
            @include('auth.layouts._logo')
            <form class="login100-form validate-form" method="POST" action="{{ route('password.email') }}">
            @csrf
                <span class="login100-form-title">Salutis Centro</span>
                <span class="login100-form-subtitle">Resetear contraseña</span>
                @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                <div class="wrap-input100 validate-input{{ $errors->has('email') ? ' has-error' : '' }}">
                    <input class="input100" type="email" name="email" value="{{ old('email') }}" placeholder="Email">
                        @include('partials.validationErrors', ['errorName' => 'email'])
                        @include('auth.layouts._iconemail')                     
                </div>
                <div class="container-login100-form-btn">
                    <button type="submit" class="login100-form-btn">Enviar Link para resetear password</button>
                    <a class="btn btn-link btn-block" href="{{ route('login') }}">Volver a Login</a>
                </div> 
            </form>
        </div>
    </div>
</div>
@endsection
