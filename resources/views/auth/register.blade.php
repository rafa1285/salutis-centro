@extends('layouts.login')
@section('content')
<div class="limiter">
    <div class="container-login100">
        <div class="wrap-login100">
            @include('auth.layouts._logo')                        
            <form class="login100-form validate-form" method="POST" action="{{ route('register') }}">                          
                @section('contentForm') 
                    <div class="wrap-input100 validate-input{{ $errors->has('name') ? ' has-error' : '' }}">               
                        <input id="name" type="text" class="input100" name="name" value="{{ old('name') }}" required autocomplete="name" placeholder="Nombre">      
                            @include('auth.layouts._iconname') 
                    </div>
                @endsection
                @section('contentForm2')
                    <div class="wrap-input100 validate-input{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                        <input id="password-confirm" type="password" class="input100" name="password_confirmation" required autocomplete="new-password" placeholder="Repite contraseña">
                        @include('partials.validationErrors', ['errorName' => 'password_confirmation'])                        
                        @include('auth.layouts._iconpassword') 
                    </div>
                @endsection
                @section('contentForm3')            
                    <a class="btn btn-link btn-block" href="{{ route('login') }}">Volver a Login</a>   
                @endsection
                @include('auth.layouts._form', ['btnText' => 'Registrar Usuario', 'subtitulo' => 'Nuevo Usuario'])                                
            </form>
        </div>
    </div>
</div>
@endsection
