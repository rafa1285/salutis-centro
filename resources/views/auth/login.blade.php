@extends('layouts.login')
@section('content')
<div class="limiter">
    <div class="container-login100">
        <div class="wrap-login100">
            @include('auth.layouts._logo')            
            <form class="login100-form validate-form" method="POST" action="{{ route('login') }}">
            @include('auth.layouts._form', ['btnText' => 'Acceder', 'subtitulo' => ''])
                <div class="text-center p-t-12">
                    <span class="txt1">¿Olvidastes</span>
                    <a class="txt2" href="{{ route('password.request') }}">Usuario / Contraseña?</a>
                </div>
            </form>
        </div>            
    </div>
</div>
@endsection
