<!-- DE MOMENTO NO TIENE USO -->
@extends('layouts.app')
@section('contenido')
<div class="row">
	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		<h3>Nuevo paciente</h3>
		<!-- Si no tenemos errores no mostramos el div -->
		@if (count($errors)>0)
		<div class="alert alert-danger">
			<ul>
				@foreach($errors -> all() as $error)
				<!-- error lo vamos a recibir de nuestro archivo request -->
				<li>{{$error}}</li>
				@endforeach
			</ul>
		</div>
		@endif
	</div>
</div>

<!-- Creamos un formulario para poder registrar los nuevos articulos
			Enviamos los parametros de nuestro formulario mediante un Array. Le enviamos la ruta especifica con la que estamos trabajando. 
			Esta ruta se ha especificado en el archivo web.php de la carpeta routes, donde le decimos que esa ruta se va a 
			encargar de trabajar con ArticuloController que es el que se encarga de la logica. El metodo al ser POST va a llamar
			a la funcion store, si es patch a update, si es delete a destroy  -->
{!!Form::open(array('url' => 'consulta/paciente', 'method' => 'POST', 'autocomplete' => 'off', 'files'=>'true'))!!}
{!!Form::token()!!}

<div class="row">
	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		<div class="form-group">
			<label for="nombre"> Nombre</label>
			<!-- name hace referencia al parametro llamado nombre que es recibido por categoria form Request -->
			<input type="text" name="nombre" required value="{{old('nombre')}}" class="form-control" placeholder="Nombre...">
		</div>
	</div>
	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		<div class="form-group">
			<label>Terapeuta</label>
			<select name="terapeuta_id" class="form-control">
				@foreach($terapeutas as $tera)
				<option value="{{$tera->id_terapeuta}}">{{$tera->nombre}}</option>
				@endforeach
			</select>
		</div>
	</div>
	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		<div class="form-group">
			<label for="codigo">Apellido 1</label>
			<!-- name hace referencia al parametro llamado nombre que es recibido por categoria form Request -->
			<input type="text" name="apellido1" required value="{{old('apellido1')}}" class="form-control" placeholder="Apellido...">
		</div>
	</div>
	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		<div class="form-group">
			<label for="nombre">Apellido 2</label>
			<!-- name hace referencia al parametro llamado nombre que es recibido por categoria form Request -->
			<input type="text" name="apellido2" value="{{old('apellido2')}}" class="form-control" placeholder="Apellido...">
		</div>
	</div>
	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		<div class="form-group">
			<label for="nombre">DNI</label>
			<!-- name hace referencia al parametro llamado nombre que es recibido por categoria form Request -->
			<input type="text" name="dni" required value="{{old('dni')}}" class="form-control" placeholder="DNI...">
		</div>
	</div>
	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		<div class="form-group">
			<label for="nombre">Fecha de nacimiento</label>
			<!-- name hace referencia al parametro llamado nombre que es recibido por categoria form Request -->
			<input type="text" name="fecha_nacimiento" required value="{{old('fecha_nacimiento')}}" class="form-control" placeholder="Fecha de nacimiento...">
		</div>
	</div>
	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		<div class="form-group">
			<label for="nombre">Direccion</label>
			<!-- name hace referencia al parametro llamado nombre que es recibido por categoria form Request -->
			<input type="text" name="direccion" required value="{{old('direccion')}}" class="form-control" placeholder="Direccion...">
		</div>
	</div>
	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		<div class="form-group">
			<label for="nombre">Telefono</label>
			<!-- name hace referencia al parametro llamado nombre que es recibido por categoria form Request -->
			<input type="text" name="telefono" required value="{{old('telefono')}}" class="form-control" placeholder="Telefono...">
		</div>
	</div>
	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		<div class="form-group">
			<label for="nombre">Email</label>
			<!-- name hace referencia al parametro llamado nombre que es recibido por categoria form Request -->
			<input type="text" name="email" required value="{{old('eamil')}}" class="form-control" placeholder="Email...">
		</div>
	</div>
	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		<div class="form-group">
			<label for="nombre">Historial</label>
			<!-- name hace referencia al parametro llamado nombre que es recibido por categoria form Request -->
			<input type="text" name="historial" required value="{{old('historial')}}" class="form-control" placeholder="Historial...">
		</div>
	</div>

	<!--
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
					<div class="form-group">
						<label for = "imagen">Imagen</label>
						<!-- name hace referencia al parametro llamado nombre que es recibido por categoria form Request 
						<input type="file" name="imagen"  class="form-control">
					</div>
				</div>-->
	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		<div class="form-group">
			<button class="btn btn-primary" type="submit">Guardar</button>
			<button class="btn btn-danger" type="reset">Cancelar</button>
		</div>
	</div>
</div>



{!!Form::close()!!}


@endsection