<!-- Esta pagina que se incluye en la vista index se va a encargar de meter una barra en la pagina de categorias que va a permitir hacer busquedas de categorias -->
<!-- Establececemos un formulario que le va a enviar datos a la pagina index para buscar una categoria. le decimos que redireccione la informacion a almacen/categoria -->
{!! Form::open(array('url' => 'cita', 'method' => 'GET', 'autocomplete' => 'off', 'role' => 'search'))!!}
<!-- Incluimos los objetos que va a tener el formulario -->
<div class="form-group">
	<div class="input-group">
		<!-- El nombre de nuestro input es el del objeto que esperamos y el del valor es del parametro que recibimos -->
		<input type="text" class="form-control" name="searchText" placeholder="Buscar..." value="{{$searchText}}">
		<span class="input-group-btn">
			<button type="submit" class="btn btn-primary">Buscar</button>
		</span>
	</div>
</div>

{{Form::close()}}