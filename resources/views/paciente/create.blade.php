<!-- Esta seccion servira para crear un nuevo paciente -->
@extends('layouts.app')
@section('contenido')
<div class="content-wrapper py-4">
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Nuevo paciente</h3>
                        </div>
                        <!-- Si no tenemos errores no mostramos el div -->
                        @if (count($errors)>0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach($errors -> all() as $error)
                                <!-- error lo vamos a recibir de nuestro archivo request -->
                                <li>{{$error}}</li>
                                @endforeach
                            </ul>
                        </div>
                        @endif
                        <!-- Creamos un formulario para poder registrar los nuevos pacientes.
						Enviamos los parametros de nuestro formulario mediante un Array. Le enviamos la ruta especifica con la que estamos trabajando. Esta ruta se ha especificado en el archivo web.php de la carpeta routes, donde le decimos que esa ruta se va a encargar de trabajar con ArticuloController que es el que se encarga de la logica. El metodo al ser POST va a llamar a la funcion store, si es patch a update, si es delete a destroy  -->
                        <form method="POST" action="{{ route('paciente.store') }}">
                        @csrf
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label for="nombre"> Nombre</label>
                                            <!-- name hace referencia al parametro llamado nombre que es recibido por categoria form Request -->
                                            <input type="text" name="nombre" required value="{{old('nombre')}}" class="form-control" placeholder="Nombre...">
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label>Terapeuta</label>
                                            <select name="terapeuta_id" class="form-control">
                                                @foreach($terapeutas as $tera)
                                                <option value="{{$tera->id_terapeuta}}">{{$tera->nombre}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label for="codigo">Apellido 1</label>
                                            <!-- name hace referencia al parametro llamado nombre que es recibido por categoria form Request -->
                                            <input type="text" name="apellido1" required value="{{old('apellido1')}}" class="form-control" placeholder="Apellido...">
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label for="nombre">Apellido 2</label>
                                            <!-- name hace referencia al parametro llamado nombre que es recibido por categoria form Request -->
                                            <input type="text" name="apellido2" value="{{old('apellido2')}}" class="form-control" placeholder="Apellido...">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label for="nombre">DNI</label>
                                            <!-- name hace referencia al parametro llamado nombre que es recibido por categoria form Request -->
                                            <input type="text" name="dni" required value="{{old('dni')}}" class="form-control" placeholder="DNI...">
                                        </div>
                                    </div>

                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label for="nombre">Fecha de nacimiento</label>
                                            <!-- name hace referencia al parametro llamado nombre que es recibido por categoria form Request -->
                                            <input class="form-control datepicker" type="text" name="fecha_nacimiento" required value="{{old('fecha_nacimiento')}}" class="form-control" placeholder="Fecha de nacimiento...">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label for="nombre">Direccion</label>
                                            <!-- name hace referencia al parametro llamado nombre que es recibido por categoria form Request -->
                                            <input type="text" name="direccion" required value="{{old('direccion')}}" class="form-control" placeholder="Direccion...">
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label for="nombre">Telefono</label>
                                            <!-- name hace referencia al parametro llamado nombre que es recibido por categoria form Request -->
                                            <input type="text" name="telefono" required value="{{old('telefono')}}" class="form-control" placeholder="Telefono...">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label for="nombre">Email</label>
                                            <!-- name hace referencia al parametro llamado nombre que es recibido por categoria form Request -->
                                            <input type="text" name="email" required value="{{old('eamil')}}" class="form-control" placeholder="Email...">
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 ">
                                        <div class="form-group">
                                            <label for="nombre">Historial</label>
                                            <!-- name hace referencia al parametro llamado nombre que es recibido por categoria form Request -->
                                            <input type="text" name="historial" required value="{{old('historial')}}" class="form-control" placeholder="Historial...">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">

                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label for="motivo_consulta">Motivo de la consulta</label>
                                            <input type="text" name="motivo_consulta" required value="{{old('motivo_consulta')}}" class="form-control" placeholder="Motivo de la consulta...">
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 py-2">
                                        <div class="form-group float-right  ">
                                            <br>
                                            <button class="btn btn-primary px-4" type="submit">Guardar</button>
                                            <button class="btn btn-danger px-4" type="reset">Cancelar</button>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.col-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container -->
    </section>	
</div>
<!-- /.wrapper -->
	
@endsection