<!-- Este index va a mostrar la pantalla de categorias con las categorias existentes -->
@extends('layouts.app')
@section('contenido')
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper py-4">
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Listado de pacientes</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
     	<table id="paciente" class="table table-bordered table-striped">
	        <thead>
                <tr>                  
					<th>Nombre</th>
					<th>Apellidos</th>
					<th>F. nacimiento</th>
					<th>Direccion</th>
					<th>Telefono</th>
					<th>Email</th>
					<th>M.consulta</th>
					<th>Terapeuta</th>
					<th>Opciones</th>
                </tr>
                </thead>
                <tbody>
                	@foreach($pacientes as $pac)
                	<tr>
                	
					<td>{{$pac -> nombre}}</td>
					<td>{{$pac -> apellido1.' ' .$pac -> apellido2}}</td>
					<td>{{$pac -> fecha_nacimiento}}</td>
					<td>{{$pac -> direccion}}</td>
					<td>{{$pac -> telefono}}</td>
					<td>{{$pac -> email}}</td>
					<td>{{$pac -> motivo_consulta}}</td>
					<td>{{$pac -> terapeuta}}</td>
					<td><a href="{{route('paciente.prueba', $pac->id_paciente)}}">
							<i class="far fa-edit px-2"></i></a>
						<a href="" data-target="#modal_delete{{$pac->id_paciente}}" data-toggle="modal">
							<i class="far fa-trash-alt px-2"></i></a></td>
				</tr>
				@include('paciente.modal')
				@endforeach
                </tbody>
                <tfoot>
                <tr>
                  
					<th>Nombre</th>
					<th>Apellidos</th>
					<th>F. nacimiento</th>
					<th>Direccion</th>
					<th>Telefono</th>
					<th>Email</th>
					<th>M.consulta</th>
					<th>Terapeuta</th>
					<th>Opciones</th>
                </tr>
                </tfoot>
		</table>
	</div>
	
</div>
</div>
</div>
</section>
@endsection