<!-- Esta seccion servira para editar una nueva categoria -->
@extends('layouts.app')
@section('contenido')
<div class="content-wrapper py-4">
	 <section class="content">
        <div class="row">
          <!-- left column -->
          <div class="col-12">
            <!-- general form elements -->
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Editar Paciente: {{ $paciente -> nombre. ' '.$paciente -> apellido1}} </h3>
                	@if (count($errors)>0)
						<div class="alert alert-danger">
							<ul>
							@foreach($errors -> all() as $error)
							{{-- error lo vamos a recibir de nuestro archivo request --}}
							<li>{{$error}}</li>
							@endforeach
							</ul>
						</div>
					@endif
              </div>
              <!-- /.card-header -->
              <!-- form start -->
			{{-- Creamos un formulario para poder editar categorias
			Enviamos los parametros de nuestro formulario. Le enviamos . El metodo al ser PATCh va a llamar a la funcion update, si es patch a update, si es delete a destroy  --}}
			<form  method="POST" action="{{ route('paciente.update', $paciente-> id_paciente) }}">
			@method('PATCH')
			@csrf 
			<div class="row">
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
					<div class="form-group">
						<label for = "nombre"> Nombre</label>
						<input type="text" name="nombre" required value="{{$paciente -> nombre}}" class="form-control">
					</div>
				</div>
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
					<div class="form-group">
						<label>Terapeuta</label>
						<select name="terapeuta_id" class="form-control">
							@foreach($terapeutas as $tera)
							@if($tera->id_terapeuta == $paciente->terapeuta_id)
								<option value="{{$tera->id_terapeuta}}"selected>{{$tera->nombre}}</option>
								@else
								<option value="{{$tera->id_terapeuta}}">{{$tera->nombre}}</option>
								@endif
							@endforeach
						</select>
					</div>
				</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
					<div class="form-group">
						<label for = "codigo">Apellido 1</label>
						<!-- name hace referencia al parametro llamado nombre que es recibido por categoria form Request -->
						<input type="text" name="apellido1" required value="{{$paciente->apellido1}}" class="form-control" placeholder="Apellido...">
					</div>
				</div>
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
					<div class="form-group">
						<label for = "nombre">Apellido 2</label>
						<!-- name hace referencia al parametro llamado nombre que es recibido por categoria form Request -->
						<input type="text" name="apellido2" value="{{$paciente->apellido2}}" class="form-control" placeholder="Apellido...">
					</div>
				</div>
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
					<div class="form-group">
						<label for = "nombre">DNI</label>
						<!-- name hace referencia al parametro llamado nombre que es recibido por categoria form Request -->
						<input type="text" name="dni" required value="{{$paciente->dni}}" class="form-control" placeholder="DNI...">
					</div>
				</div>
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
					<div class="form-group">
						<label for = "nombre">Fecha de nacimiento</label>
						<!-- name hace referencia al parametro llamado nombre que es recibido por categoria form Request -->
						<input type="text" name="fecha_nacimiento" required value="{{$paciente->fecha_nacimiento}}" class="form-control" placeholder="Fecha de nacimiento...">
					</div>
				</div>
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
					<div class="form-group">
						<label for = "nombre">Direccion</label>
						<!-- name hace referencia al parametro llamado nombre que es recibido por categoria form Request -->
						<input type="text" name="direccion" required value="{{$paciente->direccion}}" class="form-control" placeholder="Direccion...">
					</div>
				</div>
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
					<div class="form-group">
						<label for = "nombre">Telefono</label>
						<!-- name hace referencia al parametro llamado nombre que es recibido por categoria form Request -->
						<input type="text" name="telefono" required value="{{$paciente->telefono}}" class="form-control" placeholder="Telefono...">
					</div>
				</div>
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
					<div class="form-group">
						<label for = "nombre">Email</label>
						<!-- name hace referencia al parametro llamado nombre que es recibido por categoria form Request -->
						<input type="text" name="email" required value="{{$paciente->email}}" class="form-control" placeholder="Email...">
					</div>
				</div>
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
					<div class="form-group">
						<label for = "nombre">Historial</label>
						<!-- name hace referencia al parametro llamado nombre que es recibido por categoria form Request -->
						<input type="text" name="historial" required value="{{$paciente->historial}}" class="form-control" placeholder="Historial...">
					</div>
				</div>
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
					<div class="form-group">
						<label for = "nombre">Motivo de consulta</label>
						<!-- name hace referencia al parametro llamado nombre que es recibido por categoria form Request -->
						<input type="text" name="motivo_consulta" required value="{{$paciente->motivo_consulta}}" class="form-control" placeholder="Motivo de consulta...">
					</div>
				</div>
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
					<div class="form-group">
						<button class="btn btn-primary" type="submit">Guardar</button>
						<button class="btn btn-danger" type="reset">Cancelar</button>
					</div>
				</div>
			</div>
			</form>

	
@endsection