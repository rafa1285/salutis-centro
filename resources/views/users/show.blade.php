@extends('layouts.app')
@section('contenido')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper py-3">
	  <!-- Main content -->
    <section class="content">      	
    	<div class="card">
            <div class="card-header">
            	<h3 class="card-title">{{$user -> name.' ' .$user -> surname.' ' .$user -> second_surname}}</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">			
				<img class="py-2" width="100px" src="../img/fotoCV.jpg">
				<div class="row float-right py-3 px-3">
					<a href="{{route('user.edit', $user->id)}}"><i class="far fa-edit px-4" style="font-size: 25px"></i></a>
					<a href="" data-target="#modal_delete{{$user->id}}" data-toggle="modal"><i class="far fa-trash-alt px-3" style="font-size: 25px"></i></a>
					@include('users.modal')
				</div>					
				<div class="row">
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
						<div class="form-group">
							<label for = "nombre"> DNI</label>
							<!-- name hace referencia al parametro llamado nombre que es recibido por categoria form Request -->
							<input type="text" name="dni" value="{{$user -> dni}}" class="form-control"disabled style= "background-color:#FFFFFF ">
						</div>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
						<div class="form-group">
							<label for = "name"> Nombre</label>
							<!-- name hace referencia al parametro llamado nombre que es recibido por categoria form Request -->
							<input type="text" name="name" value="{{$user -> name}}" class="form-control"disabled style="background-color:#FFFFFF" >
						</div>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
						<div class="form-group">
							<label for = "surname">Apellido</label>
							<!-- name hace referencia al parametro llamado nombre que es recibido por categoria form Request -->
							<input type="text" name="surname" value="{{$user -> surname}}" class="form-control"disabled style="background-color:#FFFFFF">
						</div>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
						<div class="form-group">
							<label for = "second_surname">Segundo apellido</label>
							<!-- name hace referencia al parametro llamado nombre que es recibido por categoria form Request -->
							<input type="text" name="second_surname"  value="{{$user -> second_surname}}" class="form-control" disabled style="background-color:#FFFFFF">
						</div>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
						<div class="form-group">
							<label>Role</label>
								<input type="text" name="role"  value="{{$user->present()->roles()}}" class="form-control" disabled style="background-color:#FFFFFF">	
						</div>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
						<div class="form-group">
							<label for = "birthdate">Fecha de nacimiento</label>
							<!-- name hace referencia al parametro llamado nombre que es recibido por categoria form Request -->
							<input type="text" name="birthdate" value="{{$user -> birthdate}}" class="form-control" disabled style="background-color:#FFFFFF">
						</div>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
						<div class="form-group">
							<label for = "address">Direccion</label>
							<!-- name hace referencia al parametro llamado nombre que es recibido por categoria form Request -->
							<input type="text" name="address" value="{{$user -> address}}" class="form-control" disabled style="background-color:#FFFFFF">
						</div>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
						<div class="form-group">
							<label for = "phone">Telefono</label>
							<!-- name hace referencia al parametro llamado nombre que es recibido por categoria form Request -->
							<input type="text" name="phone" value="{{$user -> phone}}" class="form-control" disabled style="background-color:#FFFFFF">
						</div>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
						<div class="form-group">
							<label for = "email">Email</label>
							<!-- name hace referencia al parametro llamado nombre que es recibido por categoria form Request -->
							<input type="text" name="email" required value="{{$user -> email}}" class="form-control"disabled style="background-color:#FFFFFF">
						</div>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
						<div class="form-group">
							<label for = "specialities">Especialidad</label>
							<input type="text" name="specialities" value="{{$user -> present()->specialities()}}" class="form-control" disabled style="background-color:#FFFFFF">
						</div>
					</div>												
				</div>
				<!-- /.row -->
			</div>
			<!-- /.card-body -->
		</div>
		<!-- /.card -->
	</section>
</div>
@endsection