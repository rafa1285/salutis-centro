<!-- Esta seccion servira para crear un nuevo paciente -->
@extends('layouts.app')
@section('contenido')
<div class="content-wrapper py-4">
	<section class="content">
	    <!-- Main content -->
	     
	    	<div class="col-12"> 
			    <div class="card">
			        <div class="card-header">
			            <h3 class="card-title">Nuevo usuario</h3>
			        </div>
			        <div class="card-body">
				        <!-- Si no tenemos errores no mostramos el div -->
				        @if (count($errors)>0)
				        <div class="alert alert-danger">
				            <ul>
				                @foreach($errors -> all() as $error)
				                <!-- error lo vamos a recibir de nuestro archivo request -->
				                <li>{{$error}}</li>
				                @endforeach
				            </ul>
				        </div>
				        @endif
						<form  method="POST" action="{{ route('user.store') }}">
						@csrf
						@include('users.form', ['btnText' => 'Guardar', 'user' => new App\User])	
						</form>
					</div>
				</div>
			</div>		
	</section>
</div>
@endsection