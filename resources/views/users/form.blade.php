@csrf 		
<img class="py-2" width="100px" src="{{asset('img/fotoCV.jpg')}}">

<div class="row">
	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		<div class="form-group">
			<label for = "dni"> DNI</label>
			<!-- name hace referencia al parametro llamado nombre que es recibido por categoria form Request -->
			<input type="text" name="dni" required value="{{old('dni', $user->dni)}}" class="form-control">
		</div>
	</div>
	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		<div class="form-group">
			<label for = "name"> Nombre</label>
			<!-- name hace referencia al parametro llamado nombre que es recibido por categoria form Request -->
			<input type="text" name="name" required value="{{old('name', $user->name)}}" class="form-control">
		</div>
	</div>
	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		<div class="form-group">
			<label for = "surname">Apellido</label>
			<!-- name hace referencia al parametro llamado nombre que es recibido por categoria form Request -->
			<input type="text" name="surname" required value="{{old('surname', $user->surname)}}" class="form-control">
		</div>
	</div>
	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		<div class="form-group">
			<label for = "codigo">Segundo apellido</label>
			<!-- name hace referencia al parametro llamado nombre que es recibido por categoria form Request -->
			<input type="text" name="second_surname"  value="{{old('second_surname', $user -> second_surname)}}" class="form-control">
		</div>
	</div>	
	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		<div class="form-group">
			<label for = "birthdate">Fecha de nacimiento</label>
			<!-- name hace referencia al parametro llamado nombre que es recibido por categoria form Request -->
			<input type="text" name="birthdate" required value="{{old('birthdate', $user->birthdate)}}" class="form-control">
		</div>
	</div>
	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		<div class="form-group">
			<label for = "address">Direccion</label>
			<!-- name hace referencia al parametro llamado nombre que es recibido por categoria form Request -->
			<input type="text" name="address" required value="{{old('address', $user->address)}}" class="form-control">
		</div>
	</div>
	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		<div class="form-group">
			<label for = "phone">Telefono</label>
			<!-- name hace referencia al parametro llamado nombre que es recibido por categoria form Request -->
			<input type="text" name="phone" required value="{{old('phone', $user->phone)}}" class="form-control">
		</div>
	</div>
	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		<div class="form-group">
			<label for = "email">Email</label>
			<!-- name hace referencia al parametro llamado nombre que es recibido por categoria form Request -->
			<input type="text" name="email" required value="{{old('email', $user->email)}}" class="form-control">
		</div>
	</div>
	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		<div class="form-group">
			<label for = "specialities">Especialidad</label>
			<div class="checkbox">
			@foreach($specialities as $id => $name)
			<label>
				<!-- para que se marquen los roles que hay usamos el siguiente pluck-->
				<input
					type="checkbox"
					required
					value="{{$id}}"
					{{$user->specialities->pluck('id')->contains($id) ? 'checked' : ''}}
					name="specialities[]"->{{$name}}
			</label>
			@endforeach
			<br>
		</div>
		</div>
	</div>
	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		<label for="role">Roles</label>
		<div class="checkbox">
			@foreach($roles as $id => $name)
			<label>
				<!-- para que se marquen los roles que hay usamos el siguiente pluck-->
				<input
					type="checkbox"
					required
					value="{{$id}}"
					{{$user->roles->pluck('id')->contains($id) ? 'checked' : ''}}
					name="role[]"->{{$name}}
			</label>
			@endforeach
			<br>
		</div>
	</div>
	<!-- Para ocultar estos campos a la hora de actualizar, preguntamos si el usuario tiene un id. Si tiene un id significa que estamos editando y no lo muestra -->
	@unless ($user->id)
	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		<div class="form-group">
			<label for="password">Contraseña</label>
			<input class="form-control" type="password" name="password" required>
		</div>
	</div>
	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		<div class="form-group">
			<label for="password_confirmation">Confirma contraseña</label>
			<input class="form-control" type="password" name="password_confirmation" required>
		</div>
	</div>
	@endunless	
	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		<div class="form-group">
			<input class="btn btn-primary btn-lg btn-block" type="submit" value="{{$btnText}}">
		</div>
	</div>
</div>
<!-- /.row -->

