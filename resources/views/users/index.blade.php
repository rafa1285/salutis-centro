@extends('layouts.app')
@section('contenido')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper py-4">
    <!-- Main content -->
    <section class="content">
      	<div class="row">
        	<div class="col-12">
		        <div class="card">
		            <div class="card-header">
		            	<h3 class="card-title py-3">Usuarios</h3>
		              	<div class="row float-right py-3 px-3">
							<a href="{{route('user.create')}}"><i class="fas fa-plus" style="font-size: 20px"></i></a>						
						</div>
		            </div>
		            <!-- /.card-header -->
		            <div class="card-body">
		     			<table id="user" class="table table-bordered table-striped">
					        <thead>
				                <tr>
									<th>Nombre</th>
									<th>Apellidos</th>
									<th>Telefono</th>
									<th>Email</th>
									<th>Role</th>
									<th>Opciones</th>
								</tr>
							</thead>
							<tbody>								
								@foreach ($users as $user)
								<tr>
									<td>{!!$user->present()->link()!!}</td>
									<td>{{$user -> surname.' ' .$user -> second_surname}}</td>
									<td>{{$user -> phone}}</td>	
									<td>{{$user -> email}}</td>
									<td>{{$user->present()->roles()}}</td>	
									<td>										 
										<a href="{{route('user.edit', $user->id)}}"><i class="far fa-edit px-2"></i></a>
										<a href="" data-target="#modal_delete{{$user->id}}" data-toggle="modal"><i class="far fa-trash-alt px-2"></i></a>
									</td>
								</tr>
								@include('users.modal')
								@endforeach
							</tbody>					
						</table>
					</div>		
				</div>
			</div>
		</div>
	</section>
@endsection