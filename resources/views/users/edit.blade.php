@extends('layouts.app')
@section('contenido')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper py-3">
	  <!-- Main content -->
	@if(session()->has('info'))
		<div class="alert alert-success">{{session('info')}}</div>
	@endif
    <section class="content">      	
    	<div class="card">
            <div class="card-header">
            	<h3 class="card-title">Editar usuario: {{$user -> name.' ' .$user -> surname.' ' .$user -> second_surname}}</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">	
            	<form  method="POST" action="{{ route('user.update', $user-> id) }}">
				@method('PATCH')
				
				@include('users.form', ['btnText' => 'Editar'])
				</form>
			</div>
			<!-- /.card-body -->
		</div>
		<!-- /.card -->
	</section>
</div>
@endsection