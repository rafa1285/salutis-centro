<div class="modal" id="mdlCita">
	{{Form::model(array('url' => array('CitaController@citaNueva', 'method' => 'post')))}}
	{!!Form::token()!!}	
	<div class="modal-dialog" role="document" style="width:50% ">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Nueva cita</h4>
			</div><!-- /.modal-header -->
			<div class="modal-body">
				<div class="container-fluid">
					<div class="row">
						<div class="row">
							<div class="col-md-4">
								<label for="">Paciente: </label>
								<select class="selectpicker show-menu-arrow" name="paciente_id" data-style="form-control" data-live-search="true" title="-- Paciente --">
									@foreach($pacientes as $paci)
										<option value="{{$paci->nombre}}" selected>{{$paci->nombre.' ' .$paci -> apellido1.' ' .$paci -> apellido2}}										
									@endforeach
									</option>
									<input value="{{$paci->id_paciente}}" type="hidden" name="id_paciente"/>	
								</select>
							</div><!-- /.modal-col-md-3 -->
							<div class="col-md-3">
								<label for="">Fecha inicio</label>
								<input type="date" name="fecha_inicio" id="fecha_inicio" readonly class="form-control">
							</div><!-- /.modal-col-md-2 -->
							<div class="col-md-3">
								<label for="">Hora inicio</label>
								<input type="time" name="hora_inicio" id="hora_inicio" class="form-control">
							</div><!-- /.modal-col-md-2 -->
						</div>
						<div class="row">
							<div class="col-md-3 ml-auto">
								<label for="">Fecha final</label>
								<input type="date" name="fecha_final" id="fecha_final" class="form-control">
							</div><!-- /.modal-col-md-2 -->
							<div class="col-md-3 ml-auto">
								<label for="">Hora final</label>
								<input type="time" name="hora_final" id="hora_final" class="form-control">
							</div><!-- /.modal-col-md-2 -->
						</div><!-- /.modal-row -->
					</div><!-- /.modal-row -->

					<div class="modal-footer">
						<button type="submit" class="btn btn-primary">Guardar cita</button>
					</div>
				</div>
			</div><!-- /.modal-body -->
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
	{{Form::Close()}}

</div>