<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Redirige la ruta raiz a la vista login
Route::get('/', function () {
    return view('auth/login');
});

Auth::routes();

//Route::resource('terapeuta', 'TerapeutaController');
Route::resource('user', 'UserController');
Route::resource('citas','AppointmentController');
Route::resource('paciente', 'PatientController');
//Route::resource('paciente', 'PacienteController');
//Route::resource('cita', 'CitaController');

//Route::post('citaNueva','CitaController@citaNueva');


