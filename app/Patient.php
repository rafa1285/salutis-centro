<?php

namespace App;

use App\Appointment;
use App\Presenters\PatientPresenter;
use App\User;
use Illuminate\Database\Eloquent\Model;

class Patient extends Model
{
    

     protected $fillable = [        
    	'dni', 'name', 'surname', 'second_surname', 'birthdate', 'address',	'phone', 'email', 'reason_visit'
    ];

    public function note()
    {
        // En esta ocasion establecemos morphOne pk es una relacion polimorfica. Recibe como parametro la llave o prefijo que usamos al crear la migracion
        return $this->morphOne(Note::class, 'notable');
    }
     
   // public function user()
   // {
   // 	return $this->belongsTo(User::class);
   // }

    public function user()
    {
        return $this->belongsToMany(User::class);
    }

	public function appointment()
    {
        return $this->hasMany(Appointment::class);
    }

    public function present()
    {
        return new PatientPresenter($this);
    }


}
