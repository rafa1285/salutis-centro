<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Note extends Model
{
    //definimos los campos que se pueden asignar masivamente
    protected $fillable = ['note'];

    public function notable()
    {
    	//morphTo para indicar una relacion polimorfica y no le pasamos parametros
    	return $this->morphTo();
    }
}
