<?php

namespace App;

use App\Patient;
use App\User;
use Illuminate\Database\Eloquent\Model;

class Appointment extends Model
{


    public function note()
    {
        // En esta ocasion establecemos morphOne pk es una relacion polimorfica. Recibe como parametro la llave o prefijo que usamos al crear la migracion
        return $this->morphOne(Note::class, 'notable');
    }

    public function patient()
    {
    	return $this->belongsTo(Patient::class);
    }
}
