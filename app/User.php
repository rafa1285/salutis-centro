<?php

namespace App;

use App\Note;
use App\Notifications\ResetPasswordNotification;
use App\Patient;
use App\Presenters\UserPresenter;
use App\Role;
use App\Speciality;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'dni','name', 'surname', 'second_surname', 'birthdate', 'address', 'phone','email', 'password'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    //Este metodo encripta la variable password automaticamente. Desde cualquier sitio que se guarde la variable password de un usuario va a interactuar este metodo automatico
    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = bcrypt($password);
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
    }

    public function note()
    {        
        // En esta ocasion establecemos morphOne pk es una relacion polimorfica. Recibe como parametro la llave o prefijo que usamos al crear la migracion
        return $this->morphOne(Note::class, 'notable');
    }

    public function patient()
    {
        return $this->belongsToMany(Patient::class);
    }

    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }

    public function specialities()
    {
        return $this->belongsToMany(Speciality::class);
    }

    public function present()
    {
        return new UserPresenter($this);
    }

    

    
}
