<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cita extends Model
{
    protected $table = 'cita';
    protected $primaryKey = 'id_cita';
    
    protected $fillable = [
    	'paciente_id',
    	'fechaIni',
    	'fechaFin',
    	'todoeldia',
    	'color',
    	'titulo'
    ];


    protected $hidden = ['id_cita'];
}
