<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
// agregamos el modelo
use App\Paciente;
use App\Terapeuta;
// sirve para hacer algunas redirecciones
use Illuminate\Support\Facades\Redirect;
// agregamos el formulario
use App\Http\Requests\PacienteFormRequest;
// para trabajar con el espacio db de lavarel
use DB;

class PacienteController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

	// este metodo me va a obener todos los registros de la base de datos para tenerlos en el index.blade
    public function index(Request $request){
    	// si existe request voy a obtener todos los registros del paciente de nuestra base de datos
    	if($request){
    		// va a determinar el texto de busqueda para filtrar pacientes. searchText va a ser un objeto en un formulario listado para buscadr terapeutas
			$query = trim($request->get('searchText'));
			// obtengo todos los pacientes de la base de datos, la variable $pacientes se usa despues en el index.blade
    		$pacientes = DB::table('paciente as pac')
    		-> join('terapeuta as tera', 'pac.terapeuta_id', '=', 'tera.id_terapeuta') 
    		-> select('pac.id_paciente', 'pac.dni','pac.nombre', 'pac.apellido1', 'pac.apellido2', 'pac.fecha_nacimiento', 'pac.direccion', 'pac.telefono', 'pac.email', 'pac.historial', 'pac.motivo_consulta', 'pac.terapeuta_id', 'tera.nombre as terapeuta')
    		// le indicamos que el nombre almacenado en la query es el que buscamos y con los porcentajes le indicamos que puede ser al inicio o al final del texto
    		-> where ('pac.nombre', 'LIKE', '%'.$query.'%')
    		-> orwhere('pac.apellido1', 'LIKE', '%'.$query.'%')
            -> orwhere('pac.dni', 'LIKE', '%'.$query.'%')
    		-> orderby ('id_paciente', 'desc')
    		-> paginate(7);
    		// retornamos una vista que le envio una variale llamada "pacientes" la cual contiene los registros de la variale creada anteriormente $pacientes. Enviamos otro parametro llamado "serchText" que contendrá la $query 
    		return view('paciente.index', ["pacientes" => $pacientes, "searchText" => $query]);
    	}

    }

    // retornamos a una vista
    public function create()
    {
        $terapeutas=DB::table('terapeuta')->get();
        return view("paciente.create",["terapeutas"=>$terapeutas]);
    }

    //almacena los datos en la base de datos. Tenemos que validarlo y usamos pacienteFormREquest. Recibe los datos a traves de POST
    public function store(PacienteFormRequest $request){
    	// la variable despues del get hace referencia al objeto de validacion de formulario html que esta previamente validado en TerapeutaFormRequest
    	$paciente = new Paciente;
    	$paciente -> terapeuta_id=$request->get('terapeuta_id');
    	$paciente -> dni=$request -> get ('dni');    	
    	$paciente -> nombre=$request -> get ('nombre');
    	$paciente -> apellido1=$request -> get ('apellido1');
    	$paciente -> apellido2=$request -> get ('apellido2');
    	$paciente -> fecha_nacimiento=$request -> get ('fecha_nacimiento');
    	$paciente -> direccion=$request -> get ('direccion');
    	$paciente -> telefono=$request -> get ('telefono');
    	$paciente -> email=$request -> get ('email');
    	$paciente -> historial=$request -> get ('historial');
    	$paciente -> motivo_consulta=$request -> get ('motivo_consulta');
    	$paciente -> save();
    	// despues de almacenar con el metodo save nos redirecciones al listado de todos los pacientes
    	return Redirect::to('paciente');
    }

    // llamo a la funcion show que muestra el paciente que le enviamos por $id
    public function show($id){
    	// retorna a la vista indicada  enviandole la variable terapeuta que va a ser igual al modelo Terapeuta y usando la funcion findOrFail enviadole el parametro $id
    	return view("paciente.show", ["paciente"=>Paciente::findOrFail($id)]);
    }

    // llamo a la funcion edit enviandole el terapeuta que le enviamos por $id
    public function edit($id){
    	$paciente=Paciente::findOrFail($id);
    	$terapeutas=DB::table('terapeuta')->get();
    	return view("paciente.edit", ["paciente"=>$paciente, "terapeutas"=>$terapeutas]);
    }

     public function prueba($id){
        $paciente=Paciente::findOrFail($id);
        $terapeutas=DB::table('terapeuta')->get();
        return view("paciente.edit", ["paciente"=>$paciente, "terapeutas"=>$terapeutas]);
    }

    // todos los datos actualizados lo validamos previamente usando el FormRequest
    public function update(PacienteFormRequest $request, $id){
		$paciente = Paciente::findOrFail($id);
		$paciente -> terapeuta_id=$request -> get('terapeuta_id');
    	$paciente -> dni=$request -> get ('dni');    	
    	$paciente -> nombre=$request -> get ('nombre');
    	$paciente -> apellido1=$request -> get ('apellido1');
    	$paciente -> apellido2=$request -> get ('apellido2');
    	$paciente -> fecha_nacimiento=$request -> get ('fecha_nacimiento');
    	$paciente -> direccion=$request -> get ('direccion');
    	$paciente -> telefono=$request -> get ('telefono');
    	$paciente -> email=$request -> get ('email');
    	$paciente -> historial=$request -> get ('historial');
		$paciente -> motivo_consulta=$request -> get ('motivo_consulta');		
    	$paciente -> update();
    	return Redirect::to('paciente');
    }

    public function destroy($id){
    	$paciente = Paciente::findOrFail($id);
    	$paciente -> destroy($id);
    	return Redirect::to('paciente');

    }
}
