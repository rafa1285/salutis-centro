<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Paciente;
use App\Cita;
// sirve para hacer algunas redirecciones
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests;
use App\Http\Requests\CitaFormRequest;
use DB;

class CitaController extends Controller
{
     public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        if ($request) {
            // va a determinar el texto de busqueda para filtrar pacientes. searchText va a ser un objeto en un formulario listado para buscadr terapeutas
            $query = trim($request->get('searchText'));
            
            $pacientes = DB::table('paciente')->get();
        }
        return view('cita.index', ["pacientes" => $pacientes, "searchText" => $query]);
    }

    public function create()
    {
        $citas=DB::table('cita')->get();
        return view("cita.create",["citas"=>$terapeutas]);
    }

    public function citas()
    {
        $data = array(); //declaramos un array principal que va contener los datos
        $id = Cita::all()->pluck('id'); //listamos todos los id de los eventos
        $titulo = Cita::all()->pluck('titulo'); //lo mismo para lugar y fecha
        $fechaIni = Cita::all()->pluck('fechaIni');
        $fechaFin = Cita::all()->pluck('fechaFin');
        $allDay = Cita::all()->pluck('todoeldia');
        $background = Cita::all()->pluck('color');
        $count = count($id); //contamos los ids obtenidos para saber el numero exacto de eventos 

        //hacemos un ciclo para anidar los valores obtenidos a nuestro array principal $data
        for ($i = 0; $i < $count; $i++) {
            $data[$i] = array(
                "title" => $titulo[$i], //obligatoriamente "title", "start" y "url" son campos    requeridos
                "start" => $fechaIni[$i], //por el plugin asi que asignamos a cada uno el valor correspondiente
                "end" => $fechaFin[$i],
                "allDay" => $allDay[$i],
                "backgroundColor" => $background[$i],
                "id" => $id[$i]
                //"url"=>"cita".$id[$i]
                //en el campo "url" concatenamos el el URL con el id del evento para luego
                //en el evento onclick de JS hacer referencia a este y usar el método show
                //para mostrar los datos completos de un evento
            );
        }
        return response()->json($data); //para luego retornarlo y estar listo para consumirlo
    }

    public function store(Request $request)    {        
        $cita = new Cita();
        $cita -> paciente_id = $request->get('id_paciente');
        $cita->titulo = $request->get('paciente_id');
        $cita->fechaIni = $request->get('fecha_inicio'). ' ' .$request->get('hora_inicio');
        $cita->fechaFin = $request->get('fecha_final'). ' ' .$request->get('hora_final');        
        $cita->save();
        // despues de almacenar con el metodo save nos redirecciones al listado de todos los pacientes
        return Redirect::to('cita');
    }
}
