<?php

namespace App\Http\Controllers;

use App\Especialidad;
use App\Http\Requests;
use App\Http\Requests\TerapeutaFormRequest;
use App\Terapeuta;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class TerapeutaController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function index(Request $request){
    	// si existe request voy a obtener todos los registros terapetua de nuestra base de datos
    	if($request){
    		// va a determinar el texto de busqueda para filtrar terapeutas. searchText va a ser un objeto en un formulario listado para buscadr terapetuas
    		$query = trim($request->get('searchText'));
    		$terapeutas = DB::table('terapeutas as tera')
            -> join('especialidad as espe', 'tera.especialidad_id', '=', 'espe.id_especialidad')          
            -> select('tera.id_terapeuta', 'tera.dni','tera.nombre', 'tera.apellido1', 'tera.apellido2', 'tera.fecha_nacimiento', 'tera.direccion', 'tera.telefono', 'tera.email', 'espe.especialidad as especialidad') 
    		// le indicamos que el nombre almacenado en la query es el que buscamos y con los porcentajes le indicamos que puede ser al inicio o al final del texto
    		-> where ('tera.nombre', 'LIKE', '%'.$query.'%')
    		-> orwhere('tera.apellido1', 'LIKE', '%'.$query.'%')
    		-> orderby ('id_terapeuta', 'desc')
    		-> paginate(7);
    		// retornamos una vista que le envio una variale llamada "terapeutas" la cual contiene los registros de la variale creada anteriormente $terapeutas. Enviamos otro parametro llamado "serchText" que contendrá la $query 
    		return view('terapeuta.index', ["terapeutas" => $terapeutas, "searchText" => $query]);
    	}

    }

    // retornamos a una vista
    public function create(){
        $especialidades=DB::table('especialidad')->get();
        return view("terapeuta.create",["especialidades"=>$especialidades]);
    	//return view("consulta.terapeuta.create");
    }

	//almacena los datos en la base de datos. Tenemos que validarlo y usamos terapeutaFormREquest. 
	//Recibe los datos a traves de POST
    public function store(TerapeutaFormRequest $request){
    	// la variable despues del get hace referencia al objeto de validacion de formulario html que esta previamente validado en TerapeutaFormRequest
    	$terapeuta = new Terapeuta;
    	$terapeuta -> dni=$request -> get ('dni');    	
    	$terapeuta -> nombre=$request -> get ('nombre');
    	$terapeuta -> apellido1=$request -> get ('apellido1');
    	$terapeuta -> apellido2=$request -> get ('apellido2');
    	$terapeuta -> fecha_nacimiento=$request -> get ('fecha_nacimiento');
    	$terapeuta -> direccion=$request -> get ('direccion');
    	$terapeuta -> telefono=$request -> get ('telefono');
		$terapeuta -> email=$request -> get ('email');
		$terapeuta -> especialidad_id= $request -> get('especialidad_id');
    	$terapeuta -> save();
    	// despues de almacenar con el metodo save nos redirecciones al listado de todos los terapeutas
    	return Redirect::to('terapeuta');
    }

    // llamo a la funcion show que muestra el terapeuta que le enviamos por $id
    public function show($id){
    	// retorna a la vista indicada  enviandole la variable terapetua que va a ser igual al modelo Terapeuta y usando la funcion findOrFail enviadole el parametro $id
    	return view("terapeuta.show", ["terapeuta"=>Terapeuta::findOrFail($id)]);
    }

    // llamo a la funcion edit enviandole el terapetua que le enviamos por $id
    public function edit($id){
        $terapeuta=Terapeuta::findOrFail($id);
        $especialidades=Especialidad::pluck('especialidad');
        // llamo a mi formulario show y le envio la categoria con los datos a editar
        return view("terapeuta.edit", compact('terapeuta', 'especialidades'));
    }

    // todos los datos actualizados lo validamos previamente usando el FormRequest
    public function update(TerapeutaFormRequest $request, $id){
    	$terapeuta = Terapeuta::findOrFail($id);
    	$terapeuta -> dni=$request -> get ('dni');    	
    	$terapeuta -> nombre=$request -> get ('nombre');
    	$terapeuta -> apellido1=$request -> get ('apellido1');
    	$terapeuta -> apellido2=$request -> get ('apellido2');
    	$terapeuta -> fecha_nacimiento=$request -> get ('fecha_nacimiento');
    	$terapeuta -> direccion=$request -> get ('direccion');
    	$terapeuta -> telefono=$request -> get ('telefono');
    	$terapeuta -> email=$request -> get ('email');
    	$terapeuta -> especialidad_id=$request -> get ('especialidad_id');
    	$terapeuta -> update();
    	return Redirect::to('terapeuta');
    }

    public function destroy($id){
    	$terapeuta = Terapeuta::findOrFail($id);
    	//$terapeuta -> condicion = '0';
    	$terapeuta -> destroy($id);
    	return Redirect::to('terapeuta');

    }
}
