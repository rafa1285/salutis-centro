<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserFormRequest;
use App\Interfaces\UserInterface;
use App\Role;
use App\Speciality;
use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    protected $users;

    public function __construct(UserInterface $user){
        $this->users = $user;
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = $this->users->index();
        return view('users.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::pluck('display_name', 'id');
        $specialities = Speciality::pluck('display_name', 'id');
        return view('users.create', compact('roles', 'specialities'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserFormRequest $request)
    {
        $user = $this->users->store($request);
        return redirect()->route('user.index')->with('info', 'Usuario creado');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = $this->users->show($id);
        return view('users.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $roles = Role::pluck('display_name', 'id');
        $specialities = Speciality::pluck('display_name', 'id');
        $user = $this->users->show($id);
        return view('users.edit', compact('user', 'roles', 'specialities'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserFormRequest $request, $id)
    {
        //dd($id);
        $user = $this->users->update($request, $id);
        return back()->with('info', 'Usuario actualizado');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //dd($id);
        $user = $this->users->destroy($id);
        return redirect()->route('user.index');
    }
}
