<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {            
        return [
            'dni' => 'required | max:9',
            'name' => 'required | max:45',
            'surname'=> 'required | max:45',
            'second_surname'=> 'max:45',
            'role' => 'required',
            'specialities' => 'required',
            'birthdate'=> 'required | date',
            'address'=> 'required | max:100',
            'phone'=> 'required | max:11',
            'password' => 'required|confirmed', //al poner confirmado comprueba el campo password con otro llamado password_confirmation automaticamente.
            'email' => 'required|unique:users,email,'.$this->route('user')        
        ];
    }
}
