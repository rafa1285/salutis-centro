<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TerapeutaFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // hace referencia al nombre de nuestro objeto de nuestro formulario html que vamos a validar y no al campo de la base de datos
            'dni' => 'required | max:9',
            'nombre' => 'required | max:45',
            'apellido1'=> 'required | max:45',
            'apellido2'=> 'max:45',
            'fecha_nacimiento'=> 'required | date',
            'direccion'=> 'required | max:100',
            'telefono'=> 'required | max:11',
            'email'=> 'max:45'
        ];
    }
}
