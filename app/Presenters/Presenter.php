<?php

namespace App\Presenters;

use Illuminate\Database\Eloquent\Model;

// Esta clase al ser abstracta no se puede instanciar, solo servira para ser extendida y aplicara el constructor

abstract class Presenter
{

	protected $model;

	public function __construct(Model $model)
	{
		$this->model = $model;
	}

}