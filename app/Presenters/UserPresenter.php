<?php

namespace App\Presenters;

use App\User;


class UserPresenter extends Presenter
{

/* Suprimimos el constructor para no tener que hacerlo en todos los modelos. Creamos una clase abstracta Presenter con el constructor y es la que vamos a extender en todos los modelos Presenters
	protected $user;

	public function __construct(User $user)
	{
		$this->user = $user;
	}
	*/

	public function roles()
	{
		return $this->model->roles->pluck('display_name')->implode(' - ');
	}

	public function specialities()
	{
		return $this->model->specialities->pluck('display_name')->implode(' - ');
	}


	public function link()
	{
		return "<a href='" . route('user.show', $this->model->id) . "'> {$this->model->name}</a>";
	}

}


