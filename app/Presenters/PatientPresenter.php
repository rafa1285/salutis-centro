<?php

namespace App\Presenters;

use App\Patient;


class PatientPresenter extends Presenter
{

/* Suprimimos el constructor para no tener que hacerlo en todos los modelos. Creamos una clase abstracta Presenter con el constructor y es la que vamos a extender en todos los modelos Presenters
	protected $user;

	public function __construct(User $user)
	{
		$this->user = $user;
	}
	*/

	public function user()
	{
		return $this->model->user->pluck('name')->implode(' - ');
	}


	public function link()
	{
		return "<a href='" . route('paciente.show', $this->model->id) . "'> {$this->model->name}</a>";
	}

	public function notes()
	{
		return $this->model->note ? $this->model->note->body : '';
	}

}


