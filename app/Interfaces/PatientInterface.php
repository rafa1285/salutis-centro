<?php

namespace App\Interfaces;

interface PatientInterface
{
	public function index();

	public function create();

	public function store(Request $request);

	public function show($id);

	public function update($request, $id);	

	public function destroy($id);
}

