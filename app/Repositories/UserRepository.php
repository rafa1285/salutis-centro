<?php


namespace App\Repositories;
use App\Interfaces\UserInterface;
use App\Role;
use App\Speciality;
use App\User;

class UserRepository implements UserInterface
{

	public function index()
	{
        return User::with(['roles', 'specialities'])
            ->orderBy('created_at', request('sorted', 'DESC'))
            ->paginate(100);
	}

	public function create()
	{

	}

	public function show($id){
		return User::findOrFail($id);        
	}

	public function update($request, $id){
		$user = User::findOrFail($id);
		//dd($request->dni, $request->email, $request->name, $request->surname, $request->second_surname);
		
		$user->update($request->validated());
		

	}

	public function store($request){
		$user = (new User)->fill($request->all());
		$user->save();
		$user->roles()->attach($request->role);
		$user->specialities()->attach($request->specialities);
		return $user;
	}

	public function destroy($id){
		$user = User::findOrFail($id);
		 //dd($user);
		$user -> delete();
		
	}

}

