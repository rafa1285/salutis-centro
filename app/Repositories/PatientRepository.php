<?php


namespace App\Repositories;
use App\Interfaces\PatientInterface;
use App\Patient;

class PatientRepository implements PatientInterface
{

	public function index()
	{
        return Patient::with(['user','note'])
            ->orderBy('created_at', request('sorted', 'DESC'))
            ->paginate(7);
	}

	public function create()
	{

	}

	public function show($id){
		return Patient::findOrFail($id);        
	}

	public function update($request, $id){
		$user = User::findOrFail($id);
		//dd($request->dni, $request->email, $request->name, $request->surname, $request->second_surname);
		
		$user->update($request->validated());
		

	}

	public function store($request){
		$user = (new User)->fill($request->all());
		$user->save();
		$user->roles()->attach($request->role);
		$user->specialities()->attach($request->specialities);
		return $user;
	}

	public function destroy($id){
		$user = User::findOrFail($id);
		 //dd($user);
		$user -> delete();
		
	}

}

