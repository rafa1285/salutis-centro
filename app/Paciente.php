<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Paciente extends Model
{
    // creamos una variable llamada tabla que se relaciona con el nombre de la tabla que queremos mapear
    protected $table = 'paciente';
    // creamos una variable php que va a ir relacionada con el id categoria de la tabla
    protected $primaryKey = 'id_paciente';
    // Laravel incluye automaticamente el valor de creacion del registro y actualizacion. Lo establecemos a false para que no lo haga en este caso
    public $timestamps = false;

    // especificamos los campos que van a recibir un valor para rellenar en la base de datos
    protected $fillable = [        
    	'dni', 'nombre', 'apellido1', 'apellido2', 'fecha_nacimiento', 'direccion',	'telefono',	'email', 'historial', 'motivo_consulta', 'terapeuta_id'
    ];

    // estos campos se especifican cuando no queremos que se guarden en el modelo de la base de datos, a diferencia que los fillable que se guardan sin especificarlo
    protected $guarded = [
    ];
}
