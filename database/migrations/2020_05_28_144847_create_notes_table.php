<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notes', function (Blueprint $table) {
            $table->increments('id');
            $table->text('body');
            // para que solo acepte enteros y positivo unsigned.
            //Vamos a realizar relaciones polimorficas para que estas notas sea aplicable a cualquier modelo sin necesidad de ir modificando la base de datos cada vez que se decida agregar notas a un modelo
            $table->integer('notable_id')->unsigned();
            // Ahora agregamos un campo que va a indicar a que modelo pertenece esas notas
            $table->string('notable_type');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notes');
    }
}
