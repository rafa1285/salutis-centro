<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePatientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('patients', function (Blueprint $table) {
            $table->increments('id');
            $table->string('dni')->unique();
            $table->string('name');
            $table->string('surname');
            $table->string('second_surname')->nullable();
            $table->date('birthdate')->nullable();
            $table->string('address')->nullable();
            $table->integer('phone');
            $table->string('email')->unique()->nullable();
            $table->string('reason_visit')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('patients');
    }
}
