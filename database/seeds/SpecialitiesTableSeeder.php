<?php


use App\Speciality;
use Illuminate\Database\Seeder;

class SpecialitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	// Estas opciones primero deja la tabla limpia
        Speciality::truncate();

        
        for($i=1; $i < 4; $i++){
        	$speciality = Speciality::create([
        		'name' => "Especialidad {$i}",
                'display_name' => "Especialidad {$i}",
                'description' => "Especialidad {$i}"
        	]);  
        }        
    }
}
