<?php

use App\Role;
use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	// Estas opciones primero deja la tabla limpia
        User::truncate();
        Role::truncate();
        DB::table('role_user')->truncate();

        $roleA = Role::create([
                'name' => 'admin',
                'display_name' => 'Adminstrador',
                'description' => 'Administrador del sitio web'
        ]);

        $roleM = Role::create([
                'name' => 'tera',
                'display_name' => 'Terapeuta',
                'description' => 'Psicologo de la consulta'
        ]);

        $roleE = Role::create([
                'name' => 'Logo',
                'display_name' => 'Logopeda',
                'description' => 'Logopeda de la consulta'
        ]);

        for($i=1; $i < 4; $i++){
        	$user = User::create([
                'dni' => "74893254{$i}",
        		'name' => "Usuario {$i}",
                'surname' => "Apellido {$i}",
                'second_surname' => "SegundoApellido {$i}",
                'birthdate' => "2020-01-{$i}",
                'address' => "Calle {$i}",
                'phone' => "63531052{$i}",
        		'email' => "usuario{$i}@email.com",
        		'password' => '123123'

        	]);        	

        	$user->roles()->save($roleA);
        }

        for($i=4; $i < 8; $i++){
            $user = User::create([
                'dni' => "74893254{$i}",
                'name' => "Usuario {$i}",
                'surname' => "Apellido {$i}",
                'second_surname' => "SegundoApellido {$i}",
                'birthdate' => "2020-01-{$i}",
                'address' => "Calle {$i}",
                'phone' => "63531052{$i}",
                'email' => "usuario{$i}@email.com",
                'password' => '123123'

            ]);         

            $user->roles()->save($roleM);
        }

        for($i=8; $i < 15; $i++){
            $user = User::create([
                'dni' => "7489325{$i}",
                'name' => "Usuario {$i}",
                'surname' => "Apellido {$i}",
                'second_surname' => "SegundoApellido {$i}",
                'birthdate' => "2020-01-{$i}",
                'address' => "Calle {$i}",
                'phone' => "6353105{$i}",
                'email' => "usuario{$i}@email.com",
                'password' => '123123'
            ]);         

            $user->roles()->save($roleE);
        }

        
    }
}
