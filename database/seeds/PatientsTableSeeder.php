<?php


use App\Patient;
use Illuminate\Database\Seeder;

class PatientsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	// Estas opciones primero deja la tabla limpia
        Patient::truncate();        


        for($i=1; $i < 4; $i++){
        	$patient = Patient::create([
                'dni' => "74893254{$i}",
        		'name' => "Usuario {$i}",
                'surname' => "Apellido {$i}",
                'second_surname' => "SegundoApellido {$i}",
                'birthdate' => "2020-01-{$i}",
                'address' => "Calle {$i}",
                'phone' => "63531052{$i}",
        		'email' => "usuario{$i}@email.com",
        		'reason_visit' => "razon {$i}"
        	]);
        }

        for($i=4; $i < 8; $i++){
            $patient = Patient::create([
                'dni' => "74893254{$i}",
                'name' => "Usuario {$i}",
                'surname' => "Apellido {$i}",
                'second_surname' => "SegundoApellido {$i}",
                'birthdate' => "2020-01-{$i}",
                'address' => "Calle {$i}",
                'phone' => "63531052{$i}",
                'email' => "usuario{$i}@email.com",
                'reason_visit' => "razon {$i}"
            ]);  
        }

        for($i=8; $i < 15; $i++){
            $patient = Patient::create([
                'dni' => "74893254{$i}",
                'name' => "Usuario {$i}",
                'surname' => "Apellido {$i}",
                'second_surname' => "SegundoApellido {$i}",
                'birthdate' => "2020-01-{$i}",
                'address' => "Calle {$i}",
                'phone' => "6353105{$i}",
                'email' => "usuario{$i}@email.com",
                'reason_visit' => "razon {$i}"
            ]);    

        }
        
    }
}
